﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Add : ContentPage
    {
        public string name,rate,year,desc,post;
        //private SQLiteAsyncConnection _connection;
        //private ObservableCollection<Movie> _movie; //Only need this on the update page
       // public static List<Movie> movies;
        public Add()
        {
            InitializeComponent();
        }

        private async void Addmovie(object sender, EventArgs a)
        {
            name = Movname.Text.ToString();
            rate = Movrate.Text.ToString();
            year = Movyear.Text.ToString();
            desc = Movdesc.Text.ToString();
            post = Movposy.Text.ToString();
            // var movie = new Movie { Name = Movname.Text, Rating = Movrate.Text, Year = Movyear.Text, Desc = Movdesc.Text, Poster = Movposy.Text };
            //await _connection.InsertAsync(movie);

          MainPage.Addmovie(name,rate,year,desc,post);




        }
    }

}