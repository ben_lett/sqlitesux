﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Edit : ContentPage
    {
        public Edit(string name,string url, string desc, string rate, string year)
        {
            InitializeComponent();
            DetailImage.Source = new UriImageSource { Uri = new Uri(url) };
            DetailImage.Aspect = Aspect.AspectFit;
            //changes the text in .xaml to movie selected from mainpage gets data from strings above.
            MovName.Text = name;

            MovRate.Text = rate;

            MovYear.Text = year;

            MovDesc.Text = desc;


       }
        private  void confirm(object sender, EventArgs a)
        {

            
        }
    }
}