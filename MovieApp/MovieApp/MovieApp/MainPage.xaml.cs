﻿using SetupSQLite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace MovieApp
{
    public class Movie
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Rating { get; set; }
        public string Desc { get; set; }
        public string Poster { get; set; }
        
    }



    public class CollectImages
    {
        public ImageSource imgsource { get; set; }
    }

    public class ImagesAndLinks
    {
        public string allURLS { get; set; }
        public CollectImages allImages { get; set; }
    }


    public partial class MainPage : ContentPage
    {
        private static SQLiteAsyncConnection _connection;
        //private ObservableCollection<Movie> _movie; //Only need this on the update page
        public static List<Movie> movies;
        //The following fields can be set by a user interface or in a settings if you like.
        public static int NumberOfColumns = 4;
        //public static int NumberOfImages = 4;




        public List<Movie> LoadAllMovies(List<Movie> movies)
        {
            //movies.Add("ironman", "7.9", "2008", "After being held captive in an Afghan cave, billionaire engineer Tony Stark creates a unique weaponized suit of armor to fight evil.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg");
            movies.Add(new Movie { Name = "Test", Rating = "", Year = "", Desc = "", Poster = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTM0MDgwNjMyMl5BMl5BanBnXkFtZTcwNTg3NzAzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg" });
            //Addmovie("ironman 2", "7.0", "2010", "With the world now aware of his identity as Iron Man, Tony Stark must contend with both his declining health and a vengeful mad man with ties to his father's legacy.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTM0MDgwNjMyMl5BMl5BanBnXkFtZTcwNTg3NzAzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("ironman 3", "7.2", "2013", "When Tony Stark's world is torn apart by a formidable terrorist called the Mandarin, he starts an odyssey of rebuilding and retribution.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkzMjEzMjY1M15BMl5BanBnXkFtZTcwNTMxOTYyOQ@@._V1_UY268_CR3,0,182,268_AL_.jpg");
            //Addmovie("thor", "7.0", "2011", "The powerful but arrogant god Thor is cast out of Asgard to live amongst humans in Midgard (Earth), where he soon becomes one of their finest defenders.", "https://images-na.ssl-images-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("the avengers", "8.1", "2012", "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTk2NTI1MTU4N15BMl5BanBnXkFtZTcwODg0OTY0Nw@@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("the god father", "9.2", "1972", "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.", "https://images-na.ssl-images-amazon.com/images/M/MV5BZTRmNjQ1ZDYtNDgzMy00OGE0LWE4N2YtNTkzNWQ5ZDhlNGJmL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY268_CR3,0,182,268_AL_.jpg");
            //Addmovie("the god father II", "9.0", "1974", "The early life and career of Vito Corleone in 1920s New York is portrayed while his son, Michael, expands and tightens his grip on the family crime syndicate.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY268_CR3,0,182,268_AL_.jpg");
            //Addmovie("the god father III", "7.6", "1990", "In the midst of trying to legitimize his business dealings in New York and Italy in 1979, aging Mafia don Michael Corleone seeks to avow for his sins while taking his nephew Vincent Mancini under his wing.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTczMDcxNDA4MV5BMl5BanBnXkFtZTgwNjY1NTk4NjE@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("the dark knight", "9.0", "2008", "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the Dark Knight must come to terms with one of the greatest psychological tests of his ability to fight injustice.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("the dark knight rises", "8.5", "2012", "Eight s after the Joker's reign of anarchy, the Dark Knight, with the help of the enigmatic Selina, is forced from his imposed exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("batman begins", "8.3", "2005", "After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from the corruption that Scarecrow and the League of Shadows have cast upon it.", "https://images-na.ssl-images-amazon.com/images/M/MV5BNTM3OTc0MzM2OV5BMl5BanBnXkFtZTYwNzUwMTI3._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("inception", "8.8", "2010", "A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("the matrix", "8.7", "1997", "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.", "https://images-na.ssl-images-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("fight club", "8.8", "1999", "An insomniac office worker, looking for a way to change his life, crosses paths with a devil-may-care soap maker, forming an underground fight club that evolves into something much, much more.", "https://images-na.ssl-images-amazon.com/images/M/MV5BZGY5Y2RjMmItNDg5Yy00NjUwLThjMTEtNDc2OGUzNTBiYmM1XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("pulp fiction", "8.9", "1994", "The lives of two mob hit men, a boxer, a gangster's wife, and a pair of diner bandits intertwine in four tales of violence and redemption.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("se7en", "8.6", "1995", "Two detectives, a rookie and a veteran, hunt a serial killer who uses the seven deadly sins as his modus operandi.", "https://images-na.ssl-images-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("forrest gump", "8.8", "1994", "While not intelligent, Forrest Gump has accidentally been present at many historic moments, but his true love, Jenny Curran, eludes him.", "https://images-na.ssl-images-amazon.com/images/M/MV5BYThjM2MwZGMtMzg3Ny00NGRkLWE4M2EtYTBiNWMzOTY0YTI4XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UY268_CR10,0,182,268_AL_.jpg");
            //Addmovie("gladiator", "8.5", "2000", "When a Roman general is betrayed and his family murdered by an emperor's corrupt son, he comes to Rome as a gladiator to seek revenge.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMDliMmNhNDEtODUyOS00MjNlLTgxODEtN2U3NzIxMGVkZTA1L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("braveheart", "8.4", "1995", "When his secret bride is executed for assaulting an English soldier who tried to rape her, Sir William Wallace begins a revolt against King Edward I of England.", "https://images-na.ssl-images-amazon.com/images/M/MV5BNTMyNGE1ODQtYTNiNS00ZTUyLThhZjktMTgyOGZkZThlYTc3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg");
            //Addmovie("the green mile", "8.5", "1999", "The lives of guards on Death Row are affected by one of their charges: a black man accused of child murder and rape, yet who has a mysterious gift.", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1_UX182_CR0,0,182,268_AL_.jpg");

            return movies;
        }


        public static List<ImagesAndLinks> RandomImageList = randomImages(movies.Count);

        public static List<ImagesAndLinks> randomImages(int countImages)
        {
            List<ImagesAndLinks> allInformation = new List<ImagesAndLinks>();


            //Store the images in an array
            for (var i = 0; i < countImages; i++)
            {
                

                var fullurl = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg";
                //var fullurl = movies[i].Poster;

                var urlLocation = new UriImageSource { Uri = new Uri(fullurl) };
                urlLocation.CachingEnabled = false;

                allInformation.Add(new ImagesAndLinks { allURLS = fullurl, allImages = new CollectImages { imgsource = urlLocation } });
            }

            foreach (var x in allInformation)
            {
                System.Diagnostics.Debug.WriteLine(x.allURLS);
            }

            return allInformation;
        }

        public MainPage()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }


        //protected override void OnAppearing()
        //{
        //    var loadedMovies = LoadAllMovies(movies);
        //    System.Diagnostics.Debug.WriteLine($"############## BEFORE BASE {loadedMovies.Count}");
        //    //base.OnAppearing();

        //    ////ListTable();

        //    //System.Diagnostics.Debug.WriteLine($"############## BEFORE BASE {loadedMovies.Count}");



        //    ////NumberOfImages = 4;

        //    //displayImages(RandomImageList);

        //}




        public async void ListTable()
        {
            await _connection.CreateTableAsync<Movie>();
            movies = await _connection.Table<Movie>().ToListAsync();

            
        }

        public static async void Addmovie(string name, string rating, string year, string desc, string poster)
        {
            var movie = new Movie { Name = name, Rating = rating, Year = year, Desc = desc ,Poster = poster };
            await _connection.InsertAsync(movie);
        }

        void displayImages(List<ImagesAndLinks> allInformation)
        {
            Grid grid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowSpacing = 0,
                ColumnSpacing = 0,

                ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
                },

                RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star)}
                }
            };

            //Display the images on the app
            for (var i = 0; i < allInformation.Count; i++)
            {
                var img = new Image
                {
                    Source = allInformation[i].allImages.imgsource,
                    Aspect = Aspect.Fill,
                };

                var indicator = new ActivityIndicator
                {
                    IsRunning = true,
                };

                var touchImage = new TapGestureRecognizer();
                touchImage.Tapped += Tapping;
                img.GestureRecognizers.Add(touchImage);

                var c = Convert.ToInt32(i % NumberOfColumns);
                //System.Diagnostics.Debug.WriteLine($"COLUM = {c}");
                var r = Convert.ToInt32(i / NumberOfColumns);
                //System.Diagnostics.Debug.WriteLine($"ROW = {r}");

                Grid.SetColumn(img, c);
                Grid.SetRow(img, r);

                Grid.SetColumn(indicator, c);
                Grid.SetRow(indicator, r);

                grid.Children.Add
                (
                    indicator
                );

                grid.Children.Add
                (
                    img
                );
            }

            this.Content = grid;

            System.Diagnostics.Debug.WriteLine($"COUNT CHILDREN - {grid.Children.Count}");
        }

        async void Tapping(object sender, EventArgs e)
        {
            var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * NumberOfColumns;
            //System.Diagnostics.Debug.WriteLine($"COLUM = {a}");
            var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);
            //System.Diagnostics.Debug.WriteLine($"COLUM = {b}");

            //await DisplayAlert("Image URL",imgList[a + b].allURLS, "OK");
            //await pushes info from here into detailpage pushes into the strings name name2 ect ect knows what movie is selected from [a+b] which is colom pos + row pos
            await Navigation.PushAsync(new DetailPage(/*RandomImageList[a + b].allURLS,*/ movies[a + b].Poster, movies[a + b].Name, movies[a + b].Rating,movies[a + b].Year,movies[a + b].Desc));
        }
    }
}
