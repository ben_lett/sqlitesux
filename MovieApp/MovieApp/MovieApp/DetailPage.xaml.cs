﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {

        public static string passOnURL;
        public string editname, editurl,edityear,editdesc,editrate;

        public DetailPage(string name, string name2, string rating, string year, string desc)
        {
            
            InitializeComponent();
            DetailImage.Source = new UriImageSource { Uri = new Uri(name) };
            DetailImage.Aspect = Aspect.AspectFit;
            //changes the text in .xaml to movie selected from mainpage gets data from strings above.
            MovName.Text = name2;

            MovRate.Text = rating;

            MovYear.Text = year;

            MovDesc.Text = desc;

            passOnURL = name;

            editname = name2;
            editurl = name;
            editdesc = desc;
            editrate = rating;
            edityear = year;

        }
        private async void Addmov(object sender, EventArgs a)
        {

            await Navigation.PushAsync(new Add());
        }

        private async void editmov(object sender, EventArgs a)
        {
            await Navigation.PushAsync(new Edit(editname,editurl,editdesc,editrate,edityear));
        }
    }
}